
#include <cstdint>

struct timecode {

    struct number_and_user {
        uint16_t number_units : 4;
        uint16_t user_bits_field_1 : 4;       
    };

    // Frames
    number_and_user short1;
    uint16_t frames_tens : 2;
    uint16_t drop_frame_flag : 1;
    uint16_t color_frame_flag : 1;

    // Seconds
    number_and_user short2;
    uint16_t seconds_tens : 3;
    uint16_t : 1;

    // Minutes
    number_and_user short3;
    uint16_t minutes_tens : 3;
    uint16_t : 1;

    // Hours
    number_and_user short4;
    uint16_t hours_tens : 2;
    uint16_t : 2;    
    

    // Sync
    uint16_t sync = 0b0011'1111'1111'1101; 
} t;


int main() {
    return sizeof t;
}
